create table bestellung (
  bestellung_id                 integer auto_increment not null,
  kunde_kunden_id               integer not null,
  summe                         integer not null,
  constraint pk_bestellung primary key (bestellung_id)
);

create table bestellung_produkt (
  bestellung_id                 integer not null,
  produkt_id                    integer not null,
  anzahl                        integer not null,
  constraint pk_bestellung_produkt primary key (bestellung_id,produkt_id)
);

create table kunde (
  kunden_id                     integer auto_increment not null,
  vorname                       varchar(255) not null,
  nachname                      varchar(255) not null,
  strasse                       varchar(255) not null,
  hausnummer                    varchar(255) not null,
  plz                           varchar(255) not null,
  telefonnummer                 varchar(255) not null,
  constraint pk_kunde primary key (kunden_id)
);

create table produkt (
  produkt_id                    integer auto_increment not null,
  produktname                   varchar(255) not null,
  produkt_preis                 integer not null,
  constraint pk_produkt primary key (produkt_id)
);

create table retoure (
  retoure_id                    integer auto_increment not null,
  bestellung_id                 integer not null,
  erstattung                    integer not null,
  constraint uq_retoure_bestellung_id unique (bestellung_id),
  constraint pk_retoure primary key (retoure_id)
);

create table retoure_produkt (
  retoure_id                    integer not null,
  produkt_id                    integer not null,
  anzahl                        integer not null,
  constraint pk_retoure_produkt primary key (retoure_id,produkt_id)
);

create index ix_bestellung_kunde_kunden_id on bestellung (kunde_kunden_id);
alter table bestellung add constraint fk_bestellung_kunde_kunden_id foreign key (kunde_kunden_id) references kunde (kunden_id) on delete restrict on update restrict;

create index ix_bestellung_produkt_bestellung_id on bestellung_produkt (bestellung_id);
alter table bestellung_produkt add constraint fk_bestellung_produkt_bestellung_id foreign key (bestellung_id) references bestellung (bestellung_id) on delete restrict on update restrict;

create index ix_bestellung_produkt_produkt_id on bestellung_produkt (produkt_id);
alter table bestellung_produkt add constraint fk_bestellung_produkt_produkt_id foreign key (produkt_id) references produkt (produkt_id) on delete restrict on update restrict;

alter table retoure add constraint fk_retoure_bestellung_id foreign key (bestellung_id) references bestellung (bestellung_id) on delete restrict on update restrict;

create index ix_retoure_produkt_retoure_id on retoure_produkt (retoure_id);
alter table retoure_produkt add constraint fk_retoure_produkt_retoure_id foreign key (retoure_id) references retoure (retoure_id) on delete restrict on update restrict;

create index ix_retoure_produkt_produkt_id on retoure_produkt (produkt_id);
alter table retoure_produkt add constraint fk_retoure_produkt_produkt_id foreign key (produkt_id) references produkt (produkt_id) on delete restrict on update restrict;

