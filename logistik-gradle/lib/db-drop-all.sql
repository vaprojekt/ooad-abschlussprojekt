alter table bestellung drop foreign key fk_bestellung_kunde_kunden_id;
drop index ix_bestellung_kunde_kunden_id on bestellung;

alter table bestellung_produkt drop foreign key fk_bestellung_produkt_bestellung_id;
drop index ix_bestellung_produkt_bestellung_id on bestellung_produkt;

alter table bestellung_produkt drop foreign key fk_bestellung_produkt_produkt_id;
drop index ix_bestellung_produkt_produkt_id on bestellung_produkt;

alter table retoure drop foreign key fk_retoure_bestellung_id;

alter table retoure_produkt drop foreign key fk_retoure_produkt_retoure_id;
drop index ix_retoure_produkt_retoure_id on retoure_produkt;

alter table retoure_produkt drop foreign key fk_retoure_produkt_produkt_id;
drop index ix_retoure_produkt_produkt_id on retoure_produkt;

drop table if exists bestellung;

drop table if exists bestellung_produkt;

drop table if exists kunde;

drop table if exists produkt;

drop table if exists retoure;

drop table if exists retoure_produkt;

