package logistik.gradle.entity;

import io.ebean.Finder;
import io.ebean.annotation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import java.util.List;
import java.util.Objects;

@Entity
public class Bestellung {
    @Id
    private int bestellungId;
    @NotNull @ManyToOne
    private Kunde kunde;
    @OneToMany(mappedBy = "bestellung")
    private List<BestellungProdukt> posten;
    @OneToOne
    private Retoure retoure;
    @NotNull
    private int summe;

    public Bestellung(Kunde kunde) {
        this.kunde = kunde;
    }

    public int getBestellungId() {
        return this.bestellungId;
    }
    
    public int getKundenId() {
        return kunde.getKundenId();
    }
    
    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

    public List<BestellungProdukt> getPosten() {
        return this.posten;
    }

    public void setPosten(List<BestellungProdukt> posten) {
        this.posten = posten;
    }
    
    public void addPosten(BestellungProdukt bp) {
		this.posten.add(bp);
		this.summeAktualisieren();
	}
	
	public boolean removePosten(BestellungProdukt bp) {
		boolean deleted = posten.remove(bp);
		if (deleted) {
			this.summeAktualisieren();
		}
		return deleted;
	}
    
    public Retoure getRetoure() {
		return retoure;
	}

	public void setRetoure(Retoure retoure) {
		this.retoure = retoure;
	}

	public void summeAktualisieren() {
    	int zwischensumme = 0;
    	for(BestellungProdukt bp : posten) {
    		zwischensumme += bp.getKosten();
    	}
    	this.summe = zwischensumme;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bestellung other = (Bestellung) obj;
		return bestellungId == other.bestellungId && Objects.equals(kunde, other.kunde)
				&& Objects.equals(posten, other.posten) && Objects.equals(retoure, other.retoure)
				&& summe == other.summe;
	}



	public static Finder<Integer, Bestellung> find = new Finder<Integer, Bestellung>(Bestellung.class);
}
