package logistik.gradle.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.ebean.annotation.NotNull;

@Entity
public class Kunde {
    @Id
    private int kundenId;
    @NotNull
    private String vorname;
    @NotNull
    private String nachname;
    @NotNull
    private String strasse;
    @NotNull
    private String hausnummer;
    @NotNull
    private String plz;
    @NotNull
    private String telefonnummer;

    public Kunde(String vorname, String nachname, String strasse, String hausnummer, String plz, String telefonnummer) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.plz = plz;
        this.telefonnummer = telefonnummer;
    }

    public int getKundenId() {
        return kundenId;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kunde other = (Kunde) obj;
		return Objects.equals(hausnummer, other.hausnummer) && kundenId == other.kundenId
				&& Objects.equals(nachname, other.nachname) && Objects.equals(plz, other.plz)
				&& Objects.equals(strasse, other.strasse) && Objects.equals(telefonnummer, other.telefonnummer)
				&& Objects.equals(vorname, other.vorname);
	}
}
