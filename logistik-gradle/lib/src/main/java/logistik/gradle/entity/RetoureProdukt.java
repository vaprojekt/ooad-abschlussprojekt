package logistik.gradle.entity;

import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.annotation.NotNull;

@Entity
public class RetoureProdukt {
	@EmbeddedId
	private RPKey retoureProduktId;
	@ManyToOne @NotNull
	@JoinColumn(name = "retoure_id", insertable = false, updatable = false)
	private Retoure retoure;
	@ManyToOne @NotNull
	@JoinColumn(name = "produkt_id", insertable = false, updatable = false)
	private Produkt produkt;
	@NotNull
	private int anzahl;
	
	public RetoureProdukt(Retoure retoure, Produkt produkt, int anzahl) {
		this.retoureProduktId = new RPKey(retoure.getRetoureId(), produkt.getProduktId());
		this.retoure = retoure;
		this.produkt = produkt;
		this.anzahl = anzahl;
	}

	public int getRetoureId() {
		return retoure.getRetoureId();
	}

	public void setRetoure(Retoure retoure) {
		this.retoure = retoure;
	}

	public int getProduktId() {
		return produkt.getProduktId();
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	public int getErstattung() {
		return this.produkt.getProduktPreis() * this.anzahl;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetoureProdukt other = (RetoureProdukt) obj;
		return anzahl == other.anzahl && Objects.equals(produkt, other.produkt)
				&& Objects.equals(retoure, other.retoure) && Objects.equals(retoureProduktId, other.retoureProduktId);
	}



	@Embeddable
	public static class RPKey{
		private Integer retoureId;
		private Integer produktId;
		
		public RPKey(Integer retoureId, Integer produktId) {
			super();
			this.retoureId = retoureId;
			this.produktId = produktId;
		}
		@Override
		public int hashCode() {
			return retoureId + produktId;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj) 
	            return true;
	        RPKey b = (RPKey)obj;
	        if(b==null)
	            return false;
	        if (b.retoureId == retoureId && b.produktId == produktId) {
	            return true;
	        }
	        return false;
	    }
		public Integer getRetoureId() {
			return retoureId;
		}
		public void setRetoureId(Integer retoureId) {
			this.retoureId = retoureId;
		}
		public Integer getProduktId() {
			return produktId;
		}
		public void setProduktId(Integer produktId) {
			this.produktId = produktId;
		}
	}
}
