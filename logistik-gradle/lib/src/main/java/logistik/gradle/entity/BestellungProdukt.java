package logistik.gradle.entity;

import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.ebean.annotation.NotNull;

@Entity
public class BestellungProdukt {
	@EmbeddedId
	private BPKey bestellungProduktId;
	@ManyToOne @NotNull
	@JoinColumn(name = "bestellung_id", insertable = false, updatable = false)
	private Bestellung bestellung;
	@ManyToOne @NotNull
	@JoinColumn(name = "produkt_id", insertable = false, updatable = false)
	private Produkt produkt;
	@NotNull
	private int anzahl;
	
	public BestellungProdukt(Bestellung bestellung, Produkt produkt, int anzahl) {
		this.bestellungProduktId = new BPKey(bestellung.getBestellungId(), produkt.getProduktId());
		this.bestellung = bestellung;
		this.produkt = produkt;
		this.anzahl = anzahl;
	}

	public int getBestellungId() {
		return bestellung.getBestellungId();
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}

	public int getProduktId() {
		return produkt.getProduktId();
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	public int getKosten() {
		return this.produkt.getProduktPreis() * this.anzahl;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BestellungProdukt other = (BestellungProdukt) obj;
		return anzahl == other.anzahl && Objects.equals(bestellung, other.bestellung)
				&& Objects.equals(bestellungProduktId, other.bestellungProduktId)
				&& Objects.equals(produkt, other.produkt);
	}



	@Embeddable
	public static class BPKey{
		private Integer bestellungId;

	    private Integer produktId;
	    
	    public BPKey(int bestellungId, int produktId) {
	    	this.bestellungId = bestellungId;
	    	this.produktId = produktId;
	    }
	    
	    @Override
	    public int hashCode() {
	        return bestellungId + produktId;
	    }

	    @Override
	    public boolean equals(Object obj) {
	        if (this == obj) 
	            return true;
	        BPKey b = (BPKey)obj;
	        if(b==null)
	            return false;
	        if (b.bestellungId == bestellungId && b.produktId == produktId) {
	            return true;
	        }
	        return false;
	    }

		public Integer getBestellungId() {
			return bestellungId;
		}

		public void setBestellungId(Integer bestellungId) {
			this.bestellungId = bestellungId;
		}

		public Integer getProduktId() {
			return produktId;
		}

		public void setProduktId(Integer produktId) {
			this.produktId = produktId;
		}
	    
	}
}
