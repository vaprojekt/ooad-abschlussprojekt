package logistik.gradle.entity;

import io.ebean.Finder;
import io.ebean.annotation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Objects;

@Entity
public class Retoure {
    @Id
    private int retoureId;
    @NotNull @OneToOne
    private Bestellung bestellung;
    @OneToMany(mappedBy = "retoure")
    private List<RetoureProdukt> posten;
    @NotNull
    private int erstattung;

    public Retoure(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

	public int getRetoureId() {
		return this.retoureId;
	}

	public Bestellung getBestellung() {
		return this.bestellung;
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}

	public List<RetoureProdukt> getPosten() {
		return this.posten;
	}

	public void setPosten(List<RetoureProdukt> posten) {
		this.posten = posten;
	}
	
	public void addPosten(RetoureProdukt rp) {
		this.posten.add(rp);
		this.differenzAktualisieren();
	}
	
	public boolean removePosten(RetoureProdukt rp) {
		boolean deleted = posten.remove(rp);
		if (deleted) {
			this.differenzAktualisieren();
		}
		return deleted;
	}
    
	public void differenzAktualisieren() {
    	int zwischendifferenz = 0;
    	for(RetoureProdukt rp : posten) {
    		zwischendifferenz += rp.getErstattung();
    	}
    	this.erstattung = zwischendifferenz;
    }
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Retoure other = (Retoure) obj;
		return Objects.equals(bestellung, other.bestellung) && erstattung == other.erstattung
				&& Objects.equals(posten, other.posten) && retoureId == other.retoureId;
	}



	public static Finder<Integer, Retoure> find = new Finder<Integer, Retoure>(Retoure.class);
}