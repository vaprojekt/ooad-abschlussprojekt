package logistik.gradle.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.ebean.annotation.NotNull;

@Entity
public class Produkt {
    @Id
    private int produktId;
    @NotNull
    private String produktName;
    @NotNull
    private int produktPreis; //in cent

    public Produkt(String produktname, int produktPreis) {
        this.produktName = produktname;
        this.produktPreis = produktPreis;
    }

    public int getProduktId() {
        return produktId;
    }

    public String getProduktname() {
        return produktName;
    }

    public void setProduktname(String produktName) {
        this.produktName = produktName;
    }

    public int getProduktPreis() {
        return produktPreis;
    }

    public void setProduktPreis(int produktPreis) {
        this.produktPreis = produktPreis;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produkt other = (Produkt) obj;
		return produktId == other.produktId && Objects.equals(produktName, other.produktName)
				&& produktPreis == other.produktPreis;
	}
        
}
