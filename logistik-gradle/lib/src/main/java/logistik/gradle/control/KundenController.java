package logistik.gradle.control;

import java.util.regex.Pattern;

import logistik.gradle.entity.Kunde;

public class KundenController {
	
	public int createKunde(String vorname, String nachname, String strasse, String hausnummer, String plz, String telefonnummer) {
		if(vorname == null || nachname == null || strasse == null || hausnummer == null || plz == null || telefonnummer == null) {
			return -1;
		}
		if(!this.isValidPlz(plz) || !this.isValidContactNumber(hausnummer) || !this.isValidContactNumber(telefonnummer)) {
			return -1;
		}
		Kunde kunde = new Kunde(vorname, nachname, strasse, hausnummer, plz, telefonnummer);
		if(DBController.save(kunde)) {
			return kunde.getKundenId();
		} else {
			return -1;
		}
	}
	
	public Kunde readKunde(int kundenId) {
		Kunde kunde = DBController.findKunde(kundenId);
		return kunde;
	}
	
	public boolean updateKunde(int kundenId, String vorname, String nachname, String strasse, String hausnummer, String plz, String telefonnummer) {
		if(vorname == null || nachname == null || strasse == null || hausnummer == null || plz == null || telefonnummer == null) {
			return false;
		}
		if(!this.isValidPlz(plz) || !this.isValidContactNumber(hausnummer) || !this.isValidContactNumber(telefonnummer)) {
			return false;
		}
		Kunde kunde = DBController.findKunde(kundenId);
		if (kunde == null) {
			return false;
		}
		kunde.setVorname(vorname);
		kunde.setNachname(nachname);
		kunde.setStrasse(strasse);
		kunde.setHausnummer(hausnummer);
		kunde.setPlz(plz);
		kunde.setTelefonnummer(telefonnummer);
		if(DBController.save(kunde) == false) {
			return false;
		}
		Kunde vergleich = DBController.findKunde(kundenId);
		return kunde.equals(vergleich);
	}
	
	public boolean deleteKunde(int kundenId) {
		Kunde kunde = DBController.findKunde(kundenId);
		if(kunde == null) {
			return false;
		}
		return DBController.delete(kunde);
	}
	
	private boolean isValidPlz(String plz) {
		String regex = "^\\d{5}$";
		return Pattern.matches(regex, plz);
	}
	
	private boolean isValidContactNumber(String number) {
		String regex = "^[0-9].*";
		return Pattern.matches(regex, number);
	}
}
