package logistik.gradle.control;

import java.util.List;

import io.ebean.DB;
import io.ebean.Database;
import logistik.gradle.entity.Bestellung;
import logistik.gradle.entity.BestellungProdukt;
import logistik.gradle.entity.Kunde;
import logistik.gradle.entity.Produkt;
import logistik.gradle.entity.Retoure;
import logistik.gradle.entity.RetoureProdukt;

public class DBController {

	private static DBController instance = null;
	
	private Database database;
	
	private DBController() {
		instance = this;
		this.database = DB.getDefault();
	}
	
	private static void checkInstance() {
		if (instance == null) {
			new DBController();
		}
	}
	
	public static boolean save(Kunde kunde) {
		checkInstance();
		instance.database.save(kunde);
		return findKunde(kunde.getKundenId()) != null;
	}
	
	public static boolean save(Produkt produkt) {
		checkInstance();
		instance.database.save(produkt);
		return findProdukt(produkt.getProduktId()) != null;
	}
	
	public static boolean save(Bestellung bestellung) {
		checkInstance();
		instance.database.save(bestellung);
		return findProdukt(bestellung.getBestellungId()) != null;
	}
	
	public static boolean save(BestellungProdukt bestellungProdukt) {
		checkInstance();
		instance.database.save(bestellungProdukt);
		return findBestellungProdukt(bestellungProdukt.getBestellungId(), bestellungProdukt.getProduktId()) != null;
	}
	
	public static boolean save(Retoure retoure) {
		checkInstance();
		instance.database.save(retoure);
		return findRetoure(retoure.getRetoureId()) != null;
		
	}
	
	public static boolean save(RetoureProdukt retoureProdukt) {
		checkInstance();
		instance.database.save(retoureProdukt);
		return findRetoureProdukt(retoureProdukt.getRetoureId(), retoureProdukt.getProduktId()) != null;
		
	}
	
	public static Kunde findKunde(int kundenId) {
		checkInstance();
		return instance.database.find(Kunde.class).where().eq("kundenId", kundenId).findOne();
	}
	
	public static Produkt findProdukt(int produktId) {
		checkInstance();
		return instance.database.find(Produkt.class).where().eq("produktId", produktId).findOne();
	}
	
	public static Bestellung findBestellung(int bestellungId) {
		checkInstance();
		return instance.database.find(Bestellung.class).where().eq("bestellungId", bestellungId).findOne();
	}
	
	public static BestellungProdukt findBestellungProdukt(int bestellungId, int produktId) {
		checkInstance();
		return instance.database.find(BestellungProdukt.class).where().eq("bestellungId", bestellungId).and().eq("produktId", produktId).findOne();
	}
	
	public static List<BestellungProdukt> findBestellungProduktListe(int bestellungId) {
		checkInstance();
		return instance.database.find(BestellungProdukt.class).where().eq("bestellungId", bestellungId).findList();
	}
	
	public static Retoure findRetoure(int retoureId) {
		checkInstance();
		return instance.database.find(Retoure.class).where().eq("retoureId", retoureId).findOne();
	}
	
	public static RetoureProdukt findRetoureProdukt(int retoureId, int produktId) {
		checkInstance();
		return instance.database.find(RetoureProdukt.class).where().eq("retoureId", retoureId).and().eq("produktId", produktId).findOne();
	}
	
	public static List<RetoureProdukt> findRetoureProduktListe(int retoureId) {
		checkInstance();
		return instance.database.find(RetoureProdukt.class).where().eq("retoureId", retoureId).findList();
	}
	
	public static boolean delete(Kunde kunde) {
		checkInstance();
		if(findKunde(kunde.getKundenId()) == null) {
			return false;
		}
		instance.database.delete(kunde);
		return findKunde(kunde.getKundenId()) == null;
	}
	
	public static boolean delete(Produkt produkt) {
		checkInstance();
		if(findProdukt(produkt.getProduktId()) == null) {
			return false;
		}
		instance.database.delete(produkt);
		return findKunde(produkt.getProduktId()) == null;
	}
	
	public static boolean delete(Bestellung bestellung) {
		checkInstance();
		if(findBestellung(bestellung.getBestellungId()) == null) {
			return false;
		}
		instance.database.delete(bestellung);
		if (findBestellung(bestellung.getBestellungId()) != null) {
			return false;
		}
		boolean success = true;
		for(BestellungProdukt bp : bestellung.getPosten()) {
			success = delete(bp);
			if(!success) {
				break;
			}
		}
		return success;
	}
	
	public static boolean delete(BestellungProdukt bestellungProdukt) {
		checkInstance();
		if(findBestellungProdukt(bestellungProdukt.getBestellungId(), bestellungProdukt.getProduktId()) == null) {
			return false;
		}
		instance.database.delete(bestellungProdukt);
		return findBestellungProdukt(bestellungProdukt.getBestellungId(), bestellungProdukt.getProduktId()) == null;
	}
	
	public static boolean delete(Retoure retoure) {
		checkInstance();
		if(findRetoure(retoure.getRetoureId()) == null) {
			return false;
		}
		instance.database.delete(retoure);
		if (findRetoure(retoure.getRetoureId()) != null) {
			return false;
		}
		boolean success = true;
		for(RetoureProdukt rp : retoure.getPosten()) {
			success = delete(rp);
			if(!success) {
				break;
			}
		}
		return success;
	}
	
	public static boolean delete(RetoureProdukt retoureProdukt) {
		checkInstance();
		if(findRetoureProdukt(retoureProdukt.getRetoureId(), retoureProdukt.getProduktId()) == null) {
			return false;
		}
		instance.database.delete(retoureProdukt);
		return findRetoureProdukt(retoureProdukt.getRetoureId(), retoureProdukt.getProduktId()) == null;
	}
	
}
