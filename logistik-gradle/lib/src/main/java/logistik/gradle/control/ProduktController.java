package logistik.gradle.control;

import logistik.gradle.entity.Produkt;

public class ProduktController {
	
	public int createProdukt(String produktName, int produktPreis) {
		if (produktName == null || produktPreis < 0) {
			return -1;
		}
		Produkt produkt = new Produkt(produktName, produktPreis);
		if(DBController.save(produkt)) {
			return produkt.getProduktId();
		} else {
			return -1;
		}
	}
	
	public Produkt readProdukt(int produktId) {
		Produkt produkt = DBController.findProdukt(produktId);
		return produkt;
	}
	
	public boolean updateProdukt(int produktId, String produktName, int produktPreis) {
		if (produktName == null || produktPreis < 0) {
			return false;
		}
		Produkt produkt = DBController.findProdukt(produktId);
		if(produkt == null) {
			return false;
		}
		produkt.setProduktname(produktName);
		produkt.setProduktPreis(produktPreis);
		if(DBController.save(produkt)) {
			return false;
		}
		Produkt vergleich = DBController.findProdukt(produktId);
		return produkt.equals(vergleich);
	}
	
	public boolean deleteProdukt(int produktId) {
		Produkt produkt = DBController.findProdukt(produktId);
		if (produkt == null) {
			return false;
		}
		return DBController.delete(produkt);
	}
}
