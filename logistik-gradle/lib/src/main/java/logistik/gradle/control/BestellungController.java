package logistik.gradle.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import logistik.gradle.entity.Bestellung;
import logistik.gradle.entity.BestellungProdukt;

public class BestellungController {
	
	public int createBestellung(Map<Integer, Integer> posten, int kundenId) {
		KundenController kc = new KundenController();
		ProduktController pc = new ProduktController();
		if(kc.readKunde(kundenId) == null) {
			return -1;
		}
		for(int produktId : posten.values()) {
			if(pc.readProdukt(produktId) == null) {
				return -1;
			}
		}
		Bestellung bestellung = new Bestellung(kc.readKunde(kundenId));
		List<BestellungProdukt> bpListe = new ArrayList<BestellungProdukt>();
		for(Map.Entry<Integer, Integer> position : posten.entrySet()) {
			BestellungProdukt bp = new BestellungProdukt(bestellung, pc.readProdukt(position.getKey()), position.getValue());
			bpListe.add(bp);
		}
		bestellung.setPosten(bpListe);
		if(DBController.save(bestellung) == false) {
			return -1;
		}
		for(BestellungProdukt bp : bpListe) {
			bp.setBestellung(bestellung);
			if(DBController.save(bp) == false) {
				return -1;
			}
		}
		return bestellung.getBestellungId();
	}
	
	public Bestellung readBestellung(int bestellungId) {
		Bestellung bestellung = DBController.findBestellung(bestellungId);
		return bestellung;
	}
	
	public boolean updateBestellung(int bestellungId, Map<Integer, Integer> posten, int kundenId) {
		
	}
	
	public boolean deleteBestellung(int bestellungId) {
		Bestellung bestellung = DBController.findBestellung(bestellungId);
		if (bestellung == null) {
			return false;
		}
		List<BestellungProdukt> bpListe = bestellung.getPosten();
		for(BestellungProdukt bp : bpListe) {
			if(!this.deleteBestellungProdukt(bp.getBestellungId(), bp.getProduktId())) {
				return false;
			}
		}
		return DBController.delete(bestellung);
	}
}
