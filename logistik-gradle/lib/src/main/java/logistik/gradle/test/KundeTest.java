package logistik.gradle.test;

import logistik.gradle.control.DBController;
import logistik.gradle.entity.Kunde;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.ebean.DB;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

public class KundeTest {

    @Before
    public void setUp() {
        System.out.println("init ...");
    }

    @Test
    public void testCreateKunde() {
        System.out.println("running create test...");

        // Create a new Kunde
        Kunde newKunde = new Kunde("Max", "Mustermann", "Musterstr.", "12a", "12345", "0123/1234");
        DBController.save(newKunde);
        System.out.println("Created Kunde with ID: " + newKunde.getKundenId());

        // Read the created Kunde
        Kunde readKunde = DB.find(Kunde.class).where().eq("kundenId", newKunde.getKundenId()).findOne();
        assertNotNull(readKunde);
        System.out.println("Read Kunde: " + readKunde);
    }

    @Test
    public void testUpdateKunde() {
        System.out.println("running update test...");

        // Create a new Kunde
        Kunde newKunde = new Kunde("Max", "Mustermann", "Musterstr.", "12a", "12345", "0123/1234");
        DBController.save(newKunde);

        // Read the created Kunde
        Kunde readKunde = DB.find(Kunde.class).where().eq("kundenId", newKunde.getKundenId()).findOne();
        assertNotNull(readKunde);

        // Update the Kunde
        readKunde.setVorname("John");
        readKunde.setNachname("Doe");
        DBController.save(readKunde);
        System.out.println("Updated Kunde: " + readKunde);
    }

    @Test
    public void testDeleteKunde() {
        System.out.println("running delete test...");

        // Create a new Kunde
        Kunde newKunde = new Kunde("Max", "Mustermann", "Musterstr.", "12a", "12345", "0123/1234");
        DBController.save(newKunde);

        // Read the created Kunde
        Kunde readKunde = DB.find(Kunde.class).where().eq("kundenId", newKunde.getKundenId()).findOne();
        assertNotNull(readKunde);

        // Delete the Kunde
        DBController.delete(readKunde);
        assertNull(DB.find(Kunde.class, readKunde.getKundenId()));
        System.out.println("Deleted Kunde with ID: " + readKunde.getKundenId());
    }

    @After
    public void tearDown() {
        // Clean up after the test
        if (dbController != null) {
            dbController.shutdown();
        }
    }
}
