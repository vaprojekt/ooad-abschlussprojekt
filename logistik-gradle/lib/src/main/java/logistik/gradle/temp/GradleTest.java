package logistik.gradle.temp;

import java.util.ArrayList;
import java.util.List;

import io.ebean.DB;
import io.ebean.Database;
import logistik.gradle.entity.Bestellung;
import logistik.gradle.entity.BestellungProdukt;
import logistik.gradle.entity.Kunde;
import logistik.gradle.entity.Produkt;

public class GradleTest {

	public static void main(String[] args) {
		System.out.println("init ...");
        Database database = DB.getDefault();

        System.out.println("running ...");
        Kunde kunde = new Kunde("Max", "Mustermann", "Musterstr.", "12a", "12345", "0123/1234");
        database.save(kunde);
        Produkt produkt = new Produkt("Gummiente", 100);
        database.save(produkt);
        Bestellung bestellung = new Bestellung(kunde);
        List<BestellungProdukt> posten = new ArrayList<BestellungProdukt>();
        BestellungProdukt position = new BestellungProdukt(bestellung, produkt, 5);
        posten.add(position);
        bestellung.setPosten(posten);
        bestellung.summeAktualisieren();
        database.save(bestellung);
        position.setBestellung(bestellung);
        database.save(position);
        

        System.out.println("done");
	}

}
